# Summary
An ansible setup, that sets up a Local Access Point (LAP) of the data lake infrastructure currently in development at GSI. A LAP consists of two dynafed instances (Tier-1 and Tier-2) a nginx proxy and a storage endpoint (a server that is able to speak http(s) and is able to store and deliver files).
A request will be sent to the nginx proxy. This proxy is configured as a reverse proxy to handle redirects internally. It will first contact the Tier-1 dynafed instance of its own LAP. This instance redirects the request to a relevant Tier-2 dynafed instance that claims to know the file location. This instance further redirects to the storage endpoint holding the data, which is then requested and transferred to the client via the nginx reverse proxy.

![](docs/LAP_flow.png)


# Installation & Configuration
## Datalake Local Access Point Setup
* git clone https://git.gsi.de/dc/datalake-reps/ansible_lap.git
* cd ansible_lap

## Inventory Files
* Creation of YOUR_HOSTS_FILE.inv file from templates/template_hosts.inv
* Creation of dynafedInstances.inv file from templates/template_dynafedInstances.json
  * replace
    * your_dynafed_1_hostname
    * your_dynafed_1_ip
    * your_dynafed_1_port
    * your_dynafed_2_hostname
    * your_dynafed_2_ip
    * your_dynafed_2_port
    * other_dynafed_2_hostname
    * other_dynafed_2_ip
    * other_dynafed_2_port
    * your_http_storage_hostname
    * your_http_storage_ip
    * your_http_storage_port

## Credentials.yml
* Place "template_credentials" in configs folder, rename and modify. You need:
  * a root or user with root permissions
    * if necessary: create user with root permissions
    * put user public key in /home/USER/.ssh/.authorized_keys
  * http_proxy (if needed), set to none if no proxy needed.
  * dynafed_json: The location of your dynafedInstances.json
  * Xrootd is only relevant if you want to provision an xrootd dataserver as a http storage
  * nginx proxy ports for http and https: the port nginx should listen for http/https

## Running the playbook
* ansible-playbook playbook.yml -e "credentials_file=credentials_YOUR" -i YOUR_HOSTS_FILE --private-key ~/.ssh/USER_PRIVATE_KEY
* ansible-playbook playbook.yml -e "credentials_file=credentials_YOUR" -i YOUR_HOSTS_FILE --private-key ~/.ssh/USER_PRIVATE_KEY  --ask-become-pass
